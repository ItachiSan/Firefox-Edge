# Path to Firefox.

FIREFOX_PATH ?= "/c/Program Files (x86)/Mozilla Firefox/"
UNZIP ?= unzip -qq
ZIP ?= zip
MKDIR ?= mkdir -p
RM ?= rm -f
CP ?= cp
CAT ?= cat
TAIL ?= tail

default: build

# Helper targets.
fetch: fetch-toolkit fetch-browser
fetch-toolkit: omni.ja
fetch-browser: omni-browser.ja

unzip-all: unzip-global unzip-mozapps unzip-browser-browser unzip-browser-communicator
unzip: unzip-browser-browser
	rm *.ja

omni.ja:
	$(CP) $(FIREFOX_PATH)/omni.ja .
omni-browser.ja:
	$(CP) $(FIREFOX_PATH)/browser/omni.ja omni-browser.ja

unzip-%: fetch-toolkit
	$(MKDIR) chrome temp
	-$(UNZIP) omni.ja -d temp
	mv temp/chrome/toolkit/skin/classic/$* chrome/
	$(RM) -r temp

unzip-browser-%: fetch-browser
	$(MKDIR) chrome temp
	-$(UNZIP) omni-browser.ja -d temp
	mv temp/chrome/browser/skin/classic/$* chrome
	$(RM) -r temp

# Fetch the original CSS
browser-original.css: unzip-browser-browser
	$(CP) chrome/browser/browser.css browser-original.css

# Repack the extension.
build: browser-original.css stylish-theme.css
	$(TAIL) -n +2 stylish-theme.css > stylish-theme-fixed.css
	$(CAT) browser-original.css stylish-theme-fixed.css > \
		chrome/browser/browser.css
	$(ZIP) -r firefox-edge-theme@giovannisantini.tk.xpi \
		install.rdf \
		chrome.manifest \
		chrome \
		icon.png \
		preview.png
